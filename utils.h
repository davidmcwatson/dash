/*
 *  utils.h
 *
 *  A file containing useful functions for the DASH shell program
 *
 *  Author: David watson ( with some help from StackOverflow )
 */
#include <assert.h>

/*
 *  Function to split a string by a delimiter such as a space or ':'.
 *  Returns an array of char* points where each pointer points to the beginning
 *    of a new string
 *
 *  NOTE: returned pointer must be freed by calling code.
 */
char **str_split(char *a_str, const char a_delim) {
  char **result = 0;
  size_t count = 0;
  char *tmp = a_str;
  char *last_comma = 0;
  char delim[2];
  delim[0] = a_delim;
  delim[1] = 0;

  /* Count how many elements will be extracted. */
  while (*tmp) {
    if (a_delim == *tmp) {
      count++;
      last_comma = tmp;
    }
    tmp++;
  }

  /* Add space for trailing token. */
  count += last_comma < (a_str + strlen(a_str) - 1);

  /* Add space for terminating null string so caller
     knows where the list of returned strings ends. */
  count++;

  result = malloc(sizeof(char *) * count);

  if (result) {
    size_t idx = 0;
    char *token = strtok(a_str, delim);

    while (token) {
      assert(idx < count);
      *(result + idx++) = strdup(token);
      token = strtok(0, delim);
    }
    assert(idx == count - 1);
    *(result + idx) = 0;
  }

  return result;
}

/*
 *  Function to search the PATH variable for the program executable.
 *  Returns 1 on success and 0 if no match is found. The resulting full path
 *    to the executable is then stored in *(out).
 *  NOTE: *(out) pointer must be freed by calling code.
 */
int shell_find_executable( const char* program, char* PATH, char** out )
{
        char** tokens;
        char str[256];
        tokens = str_split( PATH, ':' );
        if(tokens)
        {
            int i;
            for(i = 0; *(tokens + i); i++)  // for each filePATH
            {
                int dirsize = strlen(*(tokens+i));
                int progsize = strlen(program);
                memset( str, 0, 256 );
                memcpy( str, *(tokens + i), dirsize );
                memcpy( str+dirsize+1, program, progsize );
                str[dirsize] = '/';
                // see if executable exists
                if( access( str, F_OK ) != -1 )
                {
                    *out = (char*) malloc ( strlen(str) );
                    memcpy( *out, str, strlen(str) );
                    free( tokens );
                    return 1;
                }
            }
            free( tokens );
            return 0;
        }
}
