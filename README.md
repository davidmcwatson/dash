# David Watson COMP301 Lab 1 #

## DASH ##

(D)avids (A)wful (SH)ell - An awful replacement for your shell.

### Compilation ###

All code is found in dash.c with a couple helper functions imported from utils.h, there are no dependencies other than standard C libraries.
Compilation should be as simple as `gcc -o dash dash.c` but to make it even easier, a make file exists. `make dash` will execute the compilation (assumes gcc is installed)

### Use ###

Once up and running the prompt will appear as a `> ` after the working directory. You will be able to type shell commands but
bare in mind that programs will need to be typed with their full path (e.g. `> /bin/cat`)

## Reverse.c ##

Reverse.c has been written to satisfy Task 2. Once compiled the program takes a relative file-name as a single argument and will reverse all the bytes in that file.  

### Compilation ###

All source code is found in reverse.c. `make reverse`, `make all` or simply `make` will result in reverse.c being compiled to ./reverse

### Use ###

`./reverse path/to/file.txt`
